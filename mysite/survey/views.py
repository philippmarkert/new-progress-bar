from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse,Http404, HttpResponseServerError
from django.shortcuts import redirect
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.csrf import csrf_exempt


from django import forms

import random
import urllib.parse

from .models import Participant,Results,Treatment
from .forms import DemographicForm,FinalForm

import datetime

#-------------------------------------------------------------------------------
#generate a start or fin code
def gencode(length=8):
    alpha='ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    num='0123456789'
    
    code = []
    for i in range(int(length*3/4+0.5)):
        code.append(alpha[random.randrange(0,len(alpha))])
    for i in range(int(length*1/4)):
        code.append(num[random.randrange(0,len(num))])
    
    return "".join(code)



# Helper decorator for index() to do some simple error checks
#   1) checks for startcode, and redirects to index if not in the session
#   2) retrieve the participant and result info from the DB, returns Error if not present
#   3) calls the function with additional arguments, func(request,startocode,participant,result)
def add_session_info(func):

    #this function get's called instaead of the one being decorated
    def setup(*args,**kwargs):
        request = args[0] #first argument is the request
        
        #look up the startcode in the sesion
        startcode = request.session.get('startcode', None)
        if startcode == None:
            #no startcode, redirect to login view
            return login(request)

        try:
            #get relevant DB information
            participant = Participant.objects.get(startcode=startcode)
            result = Results.objects.get(startcode=startcode)

            #append all relevant arguments, and make the function call
            return func(request,startcode,participant,result)

        except (Participant.DoesNotExist, Results.DoesNotExist) as e:
            # invlaid startcode ... shouldn't really be able to get here
            return HttpResponseServerError("Invalid session info (startcode={}), try clearing your site cookies".format(startcode))
    return setup


#---------------------------------------------------------------------------------


# In this section are views for each page of the survey. Define the order of
# them in the view_order variable above index()


# View that checks startcode, and will redicect to the next page of survey via index() if valid
def login(request):
    request.session.set_expiry(60*60) #expire sessions in 1 hour

    #render page with the startcode if it's part of the URL query string
    if request.method == "GET":
        return render(request, "login.html",{"startcode":request.GET.get("startcode",None)})

    #check the code and begin the survey, or send them to the right page if they are coming back to the survey
    if request.method == "POST":
        
        startcode = request.POST.get("startcode",None)
        try:
            #check that the participant exists for that startcode
            participant = Participant.objects.get(startcode=startcode)
            result = Results.objects.get(startcode=startcode)
        except (Participant.DoesNotExist, Results.DoesNotExist) as e:
            return render(request, "login.html",{"invalid":True})

        #save startcode in session
        request.session["startcode"] = startcode

        #incorrect opt-out, opt them in 
        if result.optout:
            result.optout = False
            result.save()

        #first log in, set progres to 1
        if participant.progress == 0: 
            participant.start_time=datetime.datetime.now()
            participant.useragent=request.META['HTTP_USER_AGENT']
            participant.progress +=1
            participant.save()
            
        #redirct to the right page
        return redirect("index")
    

#view that displays the informed consent page
def informed(request,startcode,participant,result):

    #redner the the page normally
    if request.method == "GET":
        return render(request, "informed.html")
    
    #ccheck result
    if request.method == "POST":
        consent = request.POST.get("consent",None)
        
        #notify that they have to answer
        if consent == None :
            return render(request, "informed.html",{"invalid":True})

        #if no, redirect to optout
        if consent == "no": #does not cosent, opt them out
            return redirect("/optout")
        elif consent == "yes":
            ## consent them and move them along
            result.consent=True
            participant.progress+=1
            result.save()
            participant.save()
            return redirect("index")


#view that displays the final submit page, and honesty question
def submit(request,startcode,participant,result):
    
    #render the the page normalling with appropriate form
    if request.method == "GET": 
        form = FinalForm()
        return render(request,"submit.html",{"form":form,
                                             "progress":int(participant.progress/tot_views * 100),
                                             "currentPagenumber": int(participant.progress),
                                             "totalPagenumber": int(tot_views)})
        
    #posting back, get valid results and save it
    if request.method == "POST":
        form = FinalForm(request.POST)
        if form.is_valid():
            #set the results
            result.honest = True if "Y" == form.cleaned_data["honest"] else False
            result.save()

            #move the progress forward and redirect back to index
            participant.progress+=1
            participant.save()

            return redirect("index")
        else:
            return render(request,"submit.html",{"form":form,
                                                 "progress":int(participant.progress/tot_views * 100),
                                                 "currentPagenumber": int(participant.progress),
                                                 "totalPagenumber": int(tot_views)})


#view for the final page, generate a fincode and direct users back to mturk
def finish(request,startcode,participant,result):
    if participant.fincode == None:
        participant.end_time=datetime.datetime.now()
        participant.fincode = gencode(12) #finish code
        participant.save()
    return render(request, "finish.html", {"fincode":participant.fincode})


#demographic questoin page
def demo(request,startcode,participant,result):

    #render normal page for get with the appropriate Form
    if request.method == "GET":
        form = DemographicForm()
        return render(request,"demo.html",{"form":form,
                                           "progress":int(participant.progress/tot_views * 100),
                                           "currentPagenumber": int(participant.progress),
                                           "totalPagenumber": int(tot_views)})
    
    #posting back, get valid data and save it in results
    if request.method == "POST":
        form = DemographicForm(request.POST)
        if form.is_valid():
            #set the results
            result.demo_age=form.cleaned_data["age"]
            result.demo_gender=form.cleaned_data["gender"]
            result.demo_handed=form.cleaned_data["handed"]
            result.demo_locale=form.cleaned_data["locale"]
            result.demo_edu=form.cleaned_data["education"]
            result.demo_tech=form.cleaned_data["tech"]
            result.save()
                
            #move forward the progress and redirect back to index() to get the next page
            participant.progress+=1
            participant.save()
            return redirect("index")
        else:
            #invalid data, takes them back to the form with appropriate error messaging
            return render(request,"demo.html",{"form":form,
                                               "progress":int(participant.progress/tot_views * 100),
                                               "currentPagenumber": int(participant.progress),
                                               "totalPagenumber": int(tot_views)})


#---------------------------------------------------------------------------------------

# view() Defines the order of the views that are called by the index() view. It is
# tracked by the participant.progress field. 
#
# Progress ordering is based at 1 indexing, thus the None value. If progress is
# ever 0, redirect occurs ot login() by default, and is handled by the decorator

view_order=[None,informed,demo,submit,finish]

#track how many views for progress bar
tot_views=len(view_order)

# index() is the primary entry point to the survey. The decorator will handle
# common errors, redirect to login() or HTTP errors if needed. Otherwise, the
# decorator fills in the startcode, particiapnt, and result arguments, and
# returns the result of the appropriate view. 

@add_session_info
def index(request,startcode,participant,result):
    #check the current progress counter and return the rendering for the right view
    if participant.progress < len(view_order):
        return view_order[participant.progress](request,startcode,participant,result)
    else:
        #out of bounds ... some sort of error, clear session and put them at the beginning
        request.session.flush()
        return redirect("index") 


# optout view will either redirect users back to login, if they reached this by
# accident and were not previously logged on (via the decorator), otherwise, it
# will set the optout flag, and give the option for users to change their mind
# by relogging in.

@add_session_info        
def optout(request,startcode,participant,result):
    result.optout=True
    result.save()

    request.session.flush()
    return render(request, "optout.html")
        
    

# This is the view that manages the mturk display. The decorator ensures that it
# can be loaded within a frame, as needed by MTurk. It can do multiple things,
# depending on the method:
#
#  1) GET: it will display a sample of the page, and if the correct query
#     strings are provided following a HIT acceptance, then it creates a record
#     for that worker and hit and displays a start code.
#
#  2) POST: The partciiapnt finished the survey and has a fincode and clicked
#     the submit button. This will check the code, report an error if there is
#     one, or setup a final submission via the apporpirate external submit URL
#     for MTurk into a hidden form via the mturk_submit.html template. That
#     template has javascript to activate the submission.

#enable this page to load in a FRAME!
@xframe_options_exempt
def mturk(request):

    if request.method == "POST":
        #submitting the data

        #get all relevant posted data
        assignmentId=request.POST.get("assignmentId",None)
        hitId=request.POST.get("hitId",None) 
        workerId=request.POST.get("workerId",None)
        turkSubmitTo=request.POST.get("turkSubmitTo",None)
        startcode=request.POST.get("startcode",None)
        fincode=request.POST.get("fincode",None)

        #manage the template rendering
        render_dict = {
                       'startcode':startcode,
                       'fincode':fincode,
                       "assignmentId":assignmentId,
                       "hitId":hitId,
                       "workerId":workerId,
                       "turkSubmitTo":turkSubmitTo}


        # none of these should be None/False
        if  not all((assignmentId,hitId,workerId,turkSubmitTo,startcode,fincode)):
            render_dict["error"] = "None Values"
            return render(request,"error.html",render_dict)
        
        try:
            #update the record for the participant
            participant = Participant.objects.get(workerId=workerId)

            #notify if theere is an error
            if startcode != participant.startcode or fincode != participant.fincode:
                render_dict["error"] = "Invalid startcode or fincode"
                return render(request,"error.html",render_dict)
            
            #note that they submitted
            participant.submitted = True
            participant.save()
            
            #generate apporpriate submission url
            url_vals = list(urllib.parse.urlparse(turkSubmitTo))
            url_vals[2] = "/mturk/externalSubmit"
            url_vals[4] = urllib.parse.urlencode({"assignmentId":assignmentId,
                                                  "startcode":startcode,
                                                  "fincode":fincode})
            url = urllib.parse.urlunparse(url_vals)
            
            #render the final form for submission (occurs via JS in that page)
            return render(request, "mturk_submit.html",{"action":url,
                                                        "assignmentId":assignmentId,
                                                        "startcode":startcode,
                                                        "fincode":fincode})
        except Participant.DoesNotExist:
            render_dict["error"] = "Participant Failure"
            return render(request,"error.html",render_dict)

    if request.method == "GET":

        #These URL query strings are set by MTurk 
        assignmentId=request.GET.get("assignmentId",None)
        hitId=request.GET.get("hitId",None) 
        workerId=request.GET.get("workerId",None)
        turkSubmitTo=request.GET.get("turkSubmitTo",None)

        #dictionary to drive the template
        render_dict = {'accepted':False,
                       'submitted':False,
                       'startcode':None,
                       'fincode':None,
                       "assignmentId":assignmentId,
                       "hitId":hitId,
                       "workerId":workerId,
                       "turkSubmitTo":turkSubmitTo}



        #Check to see if HIT is accepted
        if assignmentId == "ASSIGNMENT_ID_NOT_AVAILABLE" or assignmentId == None:
            return render(request, "mturk.html", render_dict)
        else:
            try:
                participant = Participant.objects.get(workerId=workerId)
            except Participant.DoesNotExist:
                #New participant

                #generate a startcode
                while(True):
                    startcode=gencode()
                    try:
                        participant = Participant.objects.get(startcode=startcode)
                    except Participant.DoesNotExist:
                        break #it's unique

                #retrieve next treatment number
                try:
                    treatment = Treatment.objects.get()
                except Treatment.DoesNotExist:
                    treatment = Treatment.objects.create(treatment=0)

                
                #create an object
                participant = Participant.objects.create(startcode=startcode,
                                                         fincode=None,
                                                         hitId=hitId,
                                                         assignmentId=assignmentId,
                                                         workerId=workerId,
                                                         turkSubmitTo=turkSubmitTo,
                                                         submitted=False,
                                                         progress=0,
                                                         treatment=treatment.treatment,
                                                         start_time=None,
                                                         end_time=None,
                )
                
                #increment the treatement counter
                treatment.treatment+=1
                #save it
                
                #create a result object!
                result = Results.objects.create(startcode=startcode)
                
                #save all the objects
                result.save()
                participant.save()
                treatment.save()
            
            #render the page with info
            render_dict['accepted'] = True
            render_dict["fincode"] = participant.fincode if participant.fincode else "" 
            render_dict["startcode"] = participant.startcode
            render_dict["submitted"] = participant.submitted


            return render(request, "mturk.html", render_dict)

@csrf_exempt
def mturk_test(request):
    #test view to do local checking
    print(request.GET)
    print(request.POST)
    return HttpResponse("<html>GET:{}<br><br>POST:{}</html>".format(request.GET,request.POST))
    

