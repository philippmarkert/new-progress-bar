from django.db import models

# Create your models here.

#this model is a global counter for treatments. Take a modulo of this for each
#treatment to determine randomized ordering
class Treatment(models.Model):
    treatment=models.IntegerField(default=0)


#This model is to represent the participant
class Participant(models.Model):
    startcode = models.CharField(max_length=8,primary_key=True) #this will be our unique identifier
    fincode = models.CharField(max_length=12,blank=True,unique=True,null=True) #this will be assigned at the end of the survey
    assignmentId=models.CharField(max_length=256,default="ASSIGNMENT_ID_NOT_AVAILABLE") #assignmentID
    hitId=models.CharField(max_length=128) # the hit this was accepted
    workerId=models.CharField(max_length=128) #the worker who accepted
    turkSubmitTo=models.CharField(max_length=256) #where we submit to at the end
    submitted=models.BooleanField(default=False) #finished and submitted
    progress=models.IntegerField(default=0) #at beginning
    treatment=models.IntegerField(default=0) #counter for treatments
    start_time=models.DateTimeField(default=None,null=True,blank=True) #start time
    end_time=models.DateTimeField(default=None,null=True,blank=True) #end time
    useragent = models.TextField(default="",blank=True) #user agent

#This model is for tracking the results
class Results(models.Model):
    startcode = models.CharField(max_length=8,primary_key=True)
    optout = models.BooleanField(default=False)
    consent = models.BooleanField(default=False)

    #demographic questions
    demo_age= models.CharField(max_length=10,default="")
    demo_gender= models.CharField(max_length=1,default="")
    demo_handed= models.CharField(max_length=1,default="")
    demo_locale= models.CharField(max_length=1,default="")
    demo_clown= models.CharField(max_length=1,default="")
    demo_edu = models.CharField(max_length=16,default="")
    demo_tech= models.CharField(max_length=16,default="")

    honest=models.BooleanField(default=False)
    #add more fields as we add more questions
