from django import forms

from django.core.exceptions import ValidationError
from django.core.validators import MaxLengthValidator,MinLengthValidator



#Each form appears on a page of its own. Check out the templates for those
# pages, plus the template for rendering a radio button entry with bootstrap.

"""Custom MultiChoiceField with a Other to handle better validations"""
class MultiChoiceFieldOther(forms.MultipleChoiceField):

    def clean(self,value):
        data = super().to_python(value)
        self.validate(data)
        self.run_validators(data)
        return data

    def validate(self,values):
        if not values and self.required:
            raise ValidationError("Please select at least one of the options",code="required")
        for v in values:
            if len(v) == 0:
                raise ValidationError("Other selection needs to be described",code="other")
            for validator in self.validators:
                validator(v)



class DemographicForm(forms.Form):

    age = forms.ChoiceField(
        label="Select your age:",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("18-24","18-24"),
                 ("25-29","25-29"),
                 ("30-34","30-34"),
                 ("35-39","35-39"),
                 ("40-44","40-44"),
                 ("45-49","45-49"),
                 ("50-54","50-54"),
                 ("54-59","54-59"),
                 ("60-64","60-64"),
                 ("65+","65+"))
    )
    
    gender = forms.ChoiceField(
        label="Select your gender",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("M","Male"),
                 ("F", "Female"),
                 ("N","Non-Identifying"))
    )

    handed = forms.ChoiceField(
        label="What is your dominate hand?",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("L","Left Handed"),
                 ("R","Right Handed"),
                 ("A","Ambidextrous"))

    )

    locale = forms.ChoiceField(
        label="Where you live is best described as",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("urban","Urban"),
                ("suburban","Suburban"),
                ("rural","Rural"))
    )

    education = forms.ChoiceField(
        label="What is the highest degree or level of school you have completed?",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("some-high","Some high school"),
		 ("high","High school"),
		 ("some-college","Some college"),
		 ("trade","Trade, technical, or vocational training"),
		 ("assoc","Associate's Degree"),
		 ("bach","Bachelor's Degree"),
		 ("master","Master's Degree"),
		 ("professional","Professional degree"),
		 ("doctorate","Doctorate"),
		 ("na","Prefer not to say"))

    )

    tech = forms.ChoiceField(
        label="Which of the following best describes your educational background or job field?",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("tech","I have an education in, or work in, the field of computer science, computer engineering or IT."),
                 ("no-tech","I do not have an education in, nor do I work in, the field of computer science, computer engineering or IT."),
                 ("na","Prefer not to say"))

    )




class FinalForm(forms.Form):

    honest=forms.ChoiceField(
        label="Please indicate if you've honestly participated in this survey and followed instructions completely. You will not be penalized/rejected for indicating 'no' but your data may not be included in the analsis:",
        required=True,
        widget=forms.RadioSelect(attrs={"class":"form-check-input"}), #this is for bootstrap
        choices=(("Y","Yes"),
                 ("N","No"))
)
    
    


        
